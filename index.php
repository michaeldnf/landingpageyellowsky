<!DOCTYPE html>
<html lang=pt-br>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<link href="https://fonts.googleapis.com/css2?family=Asap:wght@100;400;500;600&display=swap" rel="stylesheet"> 
		<link type="text/css" rel="stylesheet" href="css/style.css">
		<link rel="icon" href="images/logotipo.png">

		<title>YellowSky - Liberte o artista que existe dentro de você!</title>
	</head>
	<body>
		<?php if(isset($_GET['ok'])){ if($_GET['ok'] == 1){?>
		    <div class="modal-success">
		      <div class="modal">
		        <div class="modal-close" onclick="fecharModal()"><span>X</span></div>
		        <div class="modal-text">
		          <h2>Muito obrigado!!!!</h2>
		          <p>Foi enviado um e-mail com sua primeira aula ;-D</p>
		        </div>
		      </div>
		    </div>
		<?php }} ?>

		<header class="topo">
			<img src="images/logotipo.png">
			<div class="frase-topo">
				<h2>Liberte o artista que existe dentro de você!</h2>
			</div>
		</header>

		<section class="background">
			<div class="main">
				<div class="frase-main">
					<h2>Acesso exclusivo a aulas que farão desvendar a criatividade que existe dentro de você.</h2>
				</div>
				
				<div class="container-formulario">
					<div class="formulario">
						<h4>REALIZE SEU CADASTRO!</h4>

						<?php if(isset($_GET['ok'])){ if($_GET['ok'] == 0){ ?> 
            				<p>Preencha todos os campos!</p> 
          				<?php }} ?>

						<form method="post" action="php/post.php">
							<input type="text" name="nome" placeholder="Insira seu nome...">
							<input type="email" name="email" placeholder="Insira seu e-mail...">
							<input type="submit" value="ENVIAR">
						</form>
					</div>	
				</div>
			</div>
		</section>

		<script>
	    	function fecharModal(){
	      		document.getElementsByClassName('modal-success')[0].style.animation = "fecharModal 0.8s";
	      		document.getElementsByClassName('modal-success')[0].style.opacity = '0';
	      		document.getElementsByClassName('modal-success')[0].style.display = 'none';
	    	}
	  </script>
	</body>
</html>